
-- NOTES --
This module requires the o3d javascript library. To get this with SVN, cd to the o3d module directory, then run:
svn co http://o3d.googlecode.com/svn/trunk/samples/o3djs/ o3djs

This will get the latest version from Google, as well as put the files in the o3djs folder, as required.
